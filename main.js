let historicoMensagem = document.querySelector('#historico')
let enviar = document.querySelector('.btn_enviar')
let caixaMensagem = document.querySelector('#entrada_mensagem')
let listaMensagem = []

enviar.addEventListener('click',()=>{
    listaMensagem.push(caixaMensagem.value)
    historicoMensagem.innerHTML = criarMensagem(listaMensagem)+historicoMensagem.innerHTML
    caixaMensagem.value = ''
})

function editarMensagem(id){
    if(!caixaMensagem.value){
        caixaMensagem.value = listaMensagem[id]
        historicoMensagem.removeChild(document.querySelector(`#item${id}`))
    }else{
        alert("Texto na caixa de mensagem")
    }
    
}
function excluirMensagem(id){
    historicoMensagem.removeChild(document.querySelector(`#item${id}`))
    listaMensagem.splice(id,1)
}
function criarMensagem(listaMensagem){
    return `<li class="mensagem" id="item${listaMensagem.length - 1}">
        <p class="texto" id="mensagem${listaMensagem.length - 1}">${listaMensagem[listaMensagem.length - 1]}</p>
        <div class="buttons">
            <button class="btn_editar" onclick="editarMensagem(${listaMensagem.length - 1})" id="${listaMensagem.length - 1}" >Editar</button>
            <button class="btn_excluir" onclick="excluirMensagem(${listaMensagem.length - 1})">Excluir</button>
        </div>
     </li>
    `

}